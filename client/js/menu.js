var socket = io();

function requestJoin() {
    var gameID = document.getElementsByClassName("roomid")[0].value;
    console.log("Request for joining game fired! roomid: " + gameID);
    socket.emit("joingame", { id: gameID });
}

function requestCreate() {
    console.log("Request for game creation fired!");
    socket.emit("creategame");
}

socket.on("gotoroom", function(data) {
    window.location.href += "game?id=" + data.id;
});