var socket = io();

function GetRoomID()
{
    var ind = window.location.href.indexOf("?");
    if (ind > -1)
    {
        var params = window.location.href.split("?");
        for (var i = 0; i < params.length; i++)
        {
            if (params[i].indexOf("id=") > -1)
                return params[i].split("=")[1];
                
        }
    }
    return null;
}

function Game(canvas) 
{
    this.socket = io();
    this.canvasName = canvas;
    this.canvas = document.getElementById(canvas);
    
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
    
    this.ctx = this.canvas.getContext('2d');
    
    this.gfx = new Graphics(this.ctx);
    this.inp = new Input();

    this.board = [];/*[  [0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0],
                    [0, 0],
                    [0] ];*/
    this.rad = 45 * ( this.canvas.height / 1080);
    this.minirad = 25 * ( this.canvas.height / 1080);
    this._fontSize = 30 * ( this.canvas.height / 1080);
    this.selected = [-1, -1];
    this.currentTurn = 100;
    this.blueNums = [];// = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    this.redNums = [];// = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    this.finished = false;
    this.winner = 0;
    this.canClick = true;
    this.side = 0;

    this.gfx.SetFont(this._fontSize + "px Arial");

    window.addEventListener("resize", this.ResFix.bind(this));

    // Network stuff
    this.roomID = GetRoomID();
    this.socket.emit("entergame", {id: this.roomID});
    this.socket.on("gameinit", this.NetInit.bind(this));
    this.socket.on("serverupdate", this.NetUpdate.bind(this));

    return this;
}

Game.prototype.NetUpdate = function(data) {
    this.board[data.target.y][data.target.x] = data.target.value;
    if (this.currentTurn == 100)
        this.redNums.splice(0, 1);
    else
        this.blueNums.splice(0, 1);
        
    this.currentTurn = this.currentTurn == 100 ? 200 : 100;
    this.winner = data.winner || 0;
    if (this.winner != 0)
        this.finished = true;
}

Game.prototype.NetInit = function(data) {
    this.board = data.board;
    this.redNums = data.redNums;
    this.blueNums = data.blueNums;
    this.currentTurn = data.currentTurn;
    this.finished = data.finished;
    this.winner = data.winner;
    this.id = data.playerid;
    this.side = data.playerSide;
    //this.role = data.playerRole;
}

Game.prototype.ResFix = function(event)
{
    event.preventDefault();
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
    this.rad = 45 * ( this.canvas.height / 1080);
    this.minirad = 25 * ( this.canvas.height / 1080);
    this._fontSize = 30 * ( this.canvas.height / 1080);
    this.gfx.SetFont(this._fontSize + "px Arial");
    this.gfx.ResFix();
}

Game.prototype.Run = function() 
{
    window.requestAnimationFrame(this.Loop.bind(this));
}

Game.prototype.Loop = function() 
{
    this.Update();
    this.Draw();
    window.requestAnimationFrame(this.Loop.bind(this));
}

Game.prototype.Update = function() 
{
    var mousePos = this.inp.GetPosition();
    var centY = this.GetOffset(this.board.length, this.canvas.height, this.rad);
    var found = false;
    for (var i = 0; i < this.board.length; i++)
    {
        var centX = this.GetOffset(this.board[i].length, this.canvas.width, this.rad);
        for (var o = 0; o < this.board[i].length; o++)
        {
            var circleX = centX + this.rad + (this.rad * o * 2);
            var circleY = centY + this.rad + (this.rad * i * 1.75);
            
            if (this.GetDistance(mousePos.x, mousePos.y, circleX, circleY) < this.rad)
            {
                if (this.inp.GetButtons().button1)
                {
                    if (this.board[i][o] != 0)
                        return;
                    
                    
                    // Send mouse click
                    if (this.canClick) {
                        this.socket.emit("clientupdate", {
                            playerid: this.id,
                            target: {
                               x: o,
                               y: i
                           }
                        });
                    }
                    
                    this.canClick = false;
                    window.setTimeout((function() {
                        this.canClick = true;
                    }).bind(this), 100);
                }
                
                this.selected = [i, o];
                found = true;
                break;
            }
        }
    }
    
    if (!found)
    {
        this.selected = [-1, -1];
    }
}

Game.prototype.Draw = function() 
{
    this.gfx.Clear();

    var centY = this.GetOffset(this.board.length, this.canvas.height, this.rad);
    for (var i = 0; i < this.board.length; i++)
    {
        var centX = this.GetOffset(this.board[i].length, this.canvas.width, this.rad);
        var sub = 0;
        for (var o = 0; o < this.board[i].length; o++)
        {
            var circleX = centX + this.rad + (this.rad * o * 2);
            var circleY = centY + this.rad + (this.rad * i * 1.75);
            
            this.gfx.SetBackgroundColor(255 * (this.board[i][o] > 100 && this.board[i][o] < 200), 0, 255 * (this.board[i][o] > 200 && this.board[i][o] < 300), 255 * !(!(this.board[i][o] > 100 && this.board[i][o] < 200) && !(this.board[i][o] > 200 && this.board[i][o] < 300)));
            sub = 0 + (Math.floor(this.board[i][o] / 100) * 100);
            this.gfx.FillCircle(circleX, circleY, this.rad);
            

            this.gfx.SetBackgroundColor(0, 0, 0, 0.30 * (this.selected[0] == i && this.selected[1] == o));
            this.gfx.FillCircle(circleX, circleY, this.rad);
            
            var value = this.board[i][o] - sub;
            var textSize = this.gfx.GetTextSize(value);

            this.gfx.SetColor(0, 0, 0, 255);
            this.gfx.DrawCircle(circleX, circleY, this.rad);
            
            if (value > 0)
            {
                this.gfx.SetBackgroundColor(255, 255, 255, 255);
                this.gfx.DrawText(value, circleX, circleY);
            }
        }
    }

    var centy = this.GetOffset(this.redNums.length, this.canvas.height, this.minirad);
    for (var i = 0; i < this.redNums.length; i++) 
    {
        this.gfx.SetColor(255, 0, 0, 255);
        this.gfx.FillCircle(this.minirad * 2, centy + (this.minirad * i * 2), this.minirad);
        this.gfx.SetStrokeWidth(1 + (1 * (i == 0 && this.currentTurn == 100)));
        this.gfx.SetColor(0, 255 * (i == 0 && this.currentTurn == 100), 0, 255);
        this.gfx.DrawCircle(this.minirad * 2, centy + (this.minirad * i * 2), this.minirad);
        this.gfx.SetStrokeWidth(1);
        this.gfx.SetColor(255, 255, 255, 255);
        this.gfx.DrawText(this.redNums[i], this.minirad * 2, centy + (this.minirad * i * 2));
    }
    
    centy = this.GetOffset(this.blueNums.length, this.canvas.height, this.minirad);
    for (var i = 0; i < this.blueNums.length; i++) 
    {
        this.gfx.SetColor(0, 0, 255, 255);
        this.gfx.FillCircle(this.canvas.width - this.minirad * 2, centy + (this.minirad * i * 2), this.minirad);
        this.gfx.SetStrokeWidth(1 + (1 * (i == 0 && this.currentTurn == 200)));
        this.gfx.SetColor(0, 255 * (i == 0 && this.currentTurn == 200), 0, 255);
        this.gfx.DrawCircle(this.canvas.width - this.minirad * 2, centy + (this.minirad * i * 2), this.minirad);
        this.gfx.SetStrokeWidth(1);
        this.gfx.SetColor(255, 255, 255, 255);
        this.gfx.DrawText(this.blueNums[i], this.canvas.width - this.minirad * 2, centy + (this.minirad * i * 2));
    }
    
    if (this.finished)
    {
        var font = this.gfx.GetFont();
        this.gfx.SetFont("60px Arial");
        var messages = ["Red won!", "Blue won!", "It's a tie!"];
        var message = messages[Math.floor(this.winner / 100) - 1];
        var centerx, centery;
        centerx = (this.canvas.width / 2);
        centery = (this.canvas.height / 2);
        
        this.gfx.SetBackgroundColor(255 * (this.winner == 100), 255 * (this.winner == 300), 255 * (this.winner == 200), 255);
        this.gfx.FillRect(0, 0, this.canvas.width, this.canvas.height);

        this.gfx.SetColor(0, 0, 0, 255);
        this.gfx.SetStrokeWidth(4);
        this.gfx.StrokeText(message, centerx, centery);
        this.gfx.SetStrokeWidth(1);

        this.gfx.SetColor(255, 255, 255, 255);
        this.gfx.DrawText(message, centerx, centery);
        
        this.gfx.SetFont(font);
    }
}

Game.prototype.GetOffset = function(numOf, len, radius, customDiv = 2)
{
    var center = len / 2;
    var halfWidth = ((radius * customDiv) * numOf) / customDiv;
    return center - halfWidth;
}

Game.prototype.GetDistance = function(x1, y1, x2, y2)
{
    return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
}

function Graphics(context)
{
    this.ctx = context;
    this.ctx.textBaseline = "middle";
    this.ctx.textAlign = "center";
    
    this.ctx.miterLimit = 2;
    this.ctx.lineJoin = 'round';

    return this;
}

Graphics.prototype.Clear = function()
{
    this.ctx.clearRect(0, 0, 9999, 9999);
}

Graphics.prototype.SetColor = function(r ,g ,b, a)
{
    this.ctx.strokeStyle = "rgba(" + r + ", " + g + ", " + b + ", " + a +")";
    this.ctx.fillStyle = "rgba(" + r + ", " + g + ", " + b + ", " + a +")";
}

Graphics.prototype.ResFix = function()
{
    this.ctx.textBaseline = "middle";
    this.ctx.textAlign = "center";
}

Graphics.prototype.SetBackgroundColor = function(r, g, b, a)
{
    this.ctx.fillStyle = "rgba(" + r + ", " + g + ", " + b + ", " + a +")";
}

Graphics.prototype.SetStrokeWidth = function(width)
{
    this.ctx.lineWidth = width;
}

Graphics.prototype.DrawCircle = function(x, y, rad)
{
    this.ctx.beginPath();
    this.ctx.arc(x, y, rad, 0, 360);
    this.ctx.stroke();
}

Graphics.prototype.FillCircle = function(x, y, rad)
{
    this.ctx.beginPath();
    this.ctx.arc(x, y, rad, 0, 360);
    this.ctx.fill();
}


Graphics.prototype.DrawRect = function(x, y, w, h)
{
    this.ctx.beginPath();
    this.ctx.rect(x, y, w, h);
    this.ctx.stroke();
}

Graphics.prototype.FillRect = function(x, y, w, h)
{
    this.ctx.beginPath();
    this.ctx.rect(x, y, w, h);
    this.ctx.fill();
}

Graphics.prototype.DrawLine = function(x1, y1, x2, y2)
{
    this.ctx.beginPath();
    this.ctx.moveTo(x1, y1);
    this.ctx.lineTo(x2, y2);
    this.ctx.stroke();
}

Graphics.prototype.DrawText = function(text, x, y)
{
    this.ctx.fillText(text, x, y);
}

Graphics.prototype.StrokeText = function(text, x, y)
{
    this.ctx.strokeText(text, x, y);
}

Graphics.prototype.GetFont = function()
{
    return this.ctx.font;
}

Graphics.prototype.SetFont = function(font)
{
    this.ctx.font = font;
}

Graphics.prototype.GetTextSize = function(text)
{
    return {
        width: this.ctx.measureText(text).width,
        height: this.ctx.measureText("M").width
    };
}

function Input()
{
    this.mouse = {x: 0, y: 0, button1: false, button2: false, scroll: false};
    
    window.addEventListener("mousemove", this.MouseMoveL.bind(this));
    window.addEventListener("touchmove", this.TouchMoveL.bind(this));
    window.addEventListener("mousedown", this.MousePressL.bind(this));
    window.addEventListener("touchstart", this.TouchDownL.bind(this));
    window.addEventListener("touchend", this.TouchEndL.bind(this));
    window.addEventListener("touchcancel", this.TouchCancelL.bind(this));
    window.addEventListener("mouseup", this.MouseRelL.bind(this));
    
    return this;
}

Input.prototype.GetPosition = function()
{
    return {x: this.mouse.x, y: this.mouse.y};
}

Input.prototype.GetButtons = function() 
{
    return {button1: this.mouse.button1, button2: this.mouse.button2, middle: this.mouse.scroll };
}

Input.prototype.MouseMoveL = function(event)
{
    this.mouse.x = event.clientX;
    this.mouse.y = event.clientY;
}

Input.prototype.TouchMoveL = function(event)
{
    this.mouse.button1 = true;
    this.mouse.x = event.changedTouches[event.which].clientX;
    this.mouse.y = event.changedTouches[event.which].clientY;
}

Input.prototype.MousePressL = function(event)
{
    event.preventDefault();
    switch (event.button) {
        case 0:
            this.mouse.button1 = true;
            break;
        case 1:
            this.mouse.scroll = true;
            break;
        case 2:
            this.mouse.button2 = true;
            break;
        default:
            // code
    }
}

Input.prototype.MouseRelL = function(event)
{
    event.preventDefault();
    switch (event.button) {
        case 0:
            this.mouse.button1 = false;
            break;
        case 1:
            this.mouse.scroll = false;
            break;
        case 2:
            this.mouse.button2 = false;
            break;
        default:
            // code
    }
}

Input.prototype.TouchDownL = function(event)
{
    event.preventDefault();
    
    this.mouse.button1 = true;
    this.mouse.x = event.changedTouches[event.which].clientX;
    this.mouse.y = event.changedTouches[event.which].clientY;
}

Input.prototype.TouchEndL = function(event)
{
    event.preventDefault();
    
    this.mouse.button1 = false;
}

Input.prototype.TouchCancelL = function(event)
{
    event.preventDefault();
    
    this.mouse.button1 = false;
}
 