var socket = require("socket.io");
var express = require("express");
var app = express();
var server = require("http").Server(app);
var io = socket(server);

// Game related vars!
var games = []; // How interesting
var generatedIDs = []; // Some safety stuff
// Game related functions

function RandomBetween(min, max)
{
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function GenerateID() {
    var id = "";

    for (var i = 0; i < 8; i++)
    {
        var gen = [
            String.fromCharCode(RandomBetween(65, 90)),
            String.fromCharCode(RandomBetween(97, 122)),
            String.fromCharCode(RandomBetween(48, 57))
            ]
        id += gen[RandomBetween(0, 2)];
    }
        

    return id;
}

var Game = function(id) {
    /*
    Questioning my design decisions:
    100 -> red
    200 -> blue
    */

    this.id = id; // Unexpected id
    this.connections = [null, null]; // Current connections
    this.board = [ // Actual game board
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0],
        [0, 0],
        [0]
    ];
    this.field = [ // I hate this so much ...
        [0, 1],
        [0, -1],
        [1, 0],
        [1, -1],
        [-1, 1],
        [-1, 0]
    ];

    this.currentTurn = 100 + (100 * RandomBetween(0, 1));  // Current turn -> 100 = red, 200 = blue
    this.blueNums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]; // Turn numbers
    this.redNums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    this.finished = false;
    this.winner = 0;
    
    this.SelfDestruct = function() {
        if (this.connections.length <= 2) { // If there are no players, delete itself
            if (this.connections[0] == null && this.connections[1] == null) {
                var ind = games.indexOf(this);
                delete games[ind];
                games.splice(ind, 1);
                generatedIDs.splice(generatedIDs.indexOf(this.id), 1);
                console.log("Selfdestruct fired: " + this.id);
                //this = null; // Somewhat dangerous, maybe dont do this?
            }
        }
    }
    
    //setTimeout(this.selfDestruct.bind(this), 4000); // Delete self if there are no players and 2 seconds passed
    
    this.ClientUpdate = function(data) {
        var isRed = false;
        if (data.playerid == 0)                 // In case of the player id being null, ignore the update
            return;
    
        if (this.finished)                      // If the game is finished, dont update
            return;
            
        if (this.connections[0].id == data.playerid)    // Determines player color
            isRed = true;
        else if (this.connections[1].id == data.playerid)
            isRed = false;
        else
            return;
            
        if (isRed == true && this.currentTurn == 200)   // If its not the players turn, dont continue
            return;

        if (isRed == false && this.currentTurn == 100)
            return;

        if (data.target.y < 0 || data.target.y > this.board.length) // If the target is out of bounds, dont continue
            return;
            
        if (data.target.x < 0 || data.target.x > this.board[data.target.y])
            return;
        
        if (this.board[data.target.y][data.target.x] != 0)  // If the board piece is assigned, dont continue
            return;
        
        if (isRed) {                                        // Assigning the board piece
            if (this.redNums.length > 0) {
                this.board[data.target.y][data.target.x] = this.redNums[0] + 100;
                this.redNums.splice(0, 1);
            }
        }
        else {
            if (this.blueNums.length > 0) {
                this.board[data.target.y][data.target.x] = this.blueNums[0] + 200;
                this.blueNums.splice(0, 1);
            }
        }
        
        if (this.redNums.length == 0 && this.blueNums.length == 0) {        // Game is finished
            for (var i = 0; i < this.board.length; i++)
            for (var o = 0; o < this.board[i].length; o++)
                if (this.board[i][o] == 0)
                {
                    y = i;
                    x = o;
                }
                
            var x, y;
            var scoreRed = 0, scoreBlue = 0;
            for (var i = 0; i < this.field.length; i++)
            {
                var xx = this.field[i][1] + x
                var yy = this.field[i][0] + y
            
                if (yy >= this.board.length || yy < 0 || xx >= this.board[yy].length || xx < 0)
                    continue;
            
                if (this.board[yy][xx] >= 100 && this.board[yy][xx] <= 199) {
                    scoreRed += this.board[yy][xx] - 100;
                }
                else {
                    scoreBlue += this.board[yy][xx] - 200;
                }
            }
        
        
            this.winner = 100 + (100 * (scoreRed > scoreBlue)) + (200 * (scoreRed == scoreBlue));
            data.winner = this.winner;
        }
        
        this.currentTurn = this.currentTurn == 100 ? 200 : 100;
        //data.nextTurn = true;
        data.target.value = this.board[data.target.y][data.target.x];
        this.ServerUpdate(data);
    }
    
    this.ServerUpdate = function(newData) {
        for (var i = 0; i < this.connections.length; i++) {
            if (this.connections[i] != null)
                this.connections[i].socket.emit("serverupdate", newData);
        }
    }
    
    this.AddPlayer = function(sck) { // Adding players into the game
        var gid = GenerateID(); // Generates ID for player to use
        var ps = 0;
        /*if (this.connections.length > 2)
        {
            gid = 0;
        }*/
        if (this.connections[0] == null) {
            this.connections[0] = { id: gid, socket: sck};
            ps = 100;
        }
        else if (this.connections[1] == null) {
            this.connections[1] = { id: gid, socket: sck};
            ps = 200;
        }
        else {
            gid = 0;
            this.connections.push({ // Stores the player
                id: gid,
                socket: sck
            });
        }
        

        // Normaly, updates will be pushed as small changes, but we need to initialize the players board correctly
        // Imagine a spectator connecting in the middle of the match and the board being clear

        sck.emit("gameinit", {
            id: this.id,
            playerid: gid,
            playerSide: ps,
            board: this.board,
            redNums: this.redNums,
            blueNums: this.blueNums,
            currentTurn: this.currentTurn,
            finished: this.finished,
            winner: this.winner
        });

        sck.on("disconnect", (function() {  // Handles player disconnects
            for (var i = 0; i < this.connections.length; i++) {
                if (this.connections[i] == null)
                    continue;
                    
                if (this.connections[i].socket.id == sck.id) {
                    this.connections[i] = null;
                    if (i >= 2)
                        this.connections.splice(i, 1);
                        
                    break;
                }
            }

            if (this.connections.length <= 2) { // If there are no players, delete itself
                if (this.connections[0] == null && this.connections[1] == null) {
                    console.log("All players left, firing Selfdestruct!");
                    this.SelfDestruct();
                    //this = null; // Somewhat dangerous, maybe dont do this?
                }
            }

        }).bind(this));
        
        sck.on("clientupdate", this.ClientUpdate.bind(this));
    };

    return this;
};


app.get("/", function(req, res) {
    res.sendFile(__dirname + "/client/index.html");
});

app.get("/game", function(req, res) {
    res.sendFile(__dirname + "/client/game.html");
    //console.log("Game page connection: " + req.query.id);
});

app.use("/client", express.static(__dirname + "/client"));

server.listen(process.env.PORT || 2000);

io.sockets.on("connection", function(sck) {
    console.log("Connection");

    sck.on("creategame", function(data) {       // User requests a game creation
        console.log("Game creation request!");
        var gID = GenerateID();     // Generates id
        
        while(generatedIDs.indexOf(gID) >=0)    // If it already exists, generate another
            gID = GenerateID();
        
        generatedIDs.push(gID);     // Adds generated id to the id pool
        
        var game = new Game(gID);   // Creates a game with the new id
        games.push(game);
        
        sck.emit("gotoroom", { id: gID });  // Redirects user to game page
    });

    sck.on("joingame", function(data) {     // User requests a game connection
        console.log("Request to join game: " + data.id);
        for (var i = 0; i < games.length; i++)
        {
            if (games[i].id == data.id)     // If the current gameid is correct, redirects user to game page
            {
                // join the game
                sck.emit("gotoroom", { id: data.id });
                break;
            }
        }
    });
    
    sck.on("entergame", function(data) {    // Game page function, assigns user to game
       for (var i = 0; i < games.length; i++)
        {
            if (games[i].id == data.id)
            {
                games[i].AddPlayer(sck);
            }
        }
    });
});